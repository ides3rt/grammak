#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: Installer for the Grammak layout
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Grammak
# Last Modified: 2022-08-02
#------------------------------------------------------------------------------
# Dependencies:
#       bash
#       coreutils
#       gzip
#
# Optional Dependencies:
#       loadkmap (For Busybox-based system)
#------------------------------------------------------------------------------

declare -r prog=${0##*/}

die ()
{
	echo "$prog: $2" >&2
	(( $1 )) &&
		exit $1
}

(( $# )) &&
	die 1 "needn't arguments."

declare -i bin_err=0
for bin in chmod cp gzip loadkeys mkdir mv; {
	if ! type -P "$bin" &>/dev/null; then
		die 0 "dependency, \`$bin\`, not met."
		(( bin_err++ ))
	fi
}

(( bin_err )) &&
	die 1 "$bin_err dependency(s) missing, aborted."
unset -v bin_err

base=$(realpath "$0")
declare -r base=${base%/*}/src

[[ -d $base ]] ||
	die 1 "$base: not found."

declare -r xkb_src=$base/xkb/Xkeymap
declare -r xkb_dest=${XDG_CONFIG_HOME:=$HOME/.config}/X11

tty_src=$base/console/grammak.map

if type -P loadkmap &>/dev/null; then
	loadkeys -b "$tty_src" > "${tty_src%.map}".bmap
	tty_src=${tty_src%.map}.bmap
	tty_dest=/etc/keymap

elif [[ -d /usr/share/kbd ]]; then
	tty_dest=/usr/share/kbd/keymaps/i386/grammak

else
	tty_dest=/usr/share/keymaps
fi

declare -r tty_src tty_dest

declare -i err=0
for f in "$xkb_src" "$tty_src"; {
	if ! [[ -f $f && -r $f ]]; then
		die 0 "$f: not found or unreadable."
		(( err++ ))
	fi
}

(( err )) &&
	die 1 "$err file(s) not found or unreadable, aborted."

trap 'die 0 "installation was finished with $err error(s)."; exit $err' EXIT

[[ -d $xkb_dest ]] ||
	mkdir -p -- "$xkb_dest"

cp -- "$xkb_src" "$xkb_dest"

(( EUID )) &&
	exit

if ! {
	mkdir -p -- "$tty_dest" &&
	gzip -k -- "$tty_src" &&
	mv -- "$tty_src".gz "$tty_dest" &&
	chmod 644 "$tty_dest/${tty_src##*/}".gz
}; then
	die 0 "${tty_src##*/}: installation failed."
	(( err++ ))
fi
